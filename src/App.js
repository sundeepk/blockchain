import React, { Component } from 'react';
import './App.css';
import SignUp from "./screens/SignUp";
import SignIn from "./screens/SignIn";
import Land from "./screens/Land";
import { Link, Redirect, Route, BrowserRouter as Router } from "react-router-dom"
import 'react-table/react-table.css'

// import { auth, base } from "./utils/base"

// Authorization HOC
const Authorization = (WrappedComponent, allowedRoles, user, userRole) => {
  return class WithAuthorization extends React.Component {
    constructor(props) {
      super(props)

      // In this case the user is hardcoded, but it could be loaded from anywhere.
      // Redux, MobX, RxJS, Backbone...
      this.state = {
        
      }

    }

    render() {
      const role = userRole
      if (allowedRoles.includes(role)) {
        return <WrappedComponent {...this.props} user={user} userRole={userRole} />
      } 
      // else {
      //   return <Redirect to="/home" />
      // }
    }
  }
}

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      user: {},
      userRole: '',
      activeItem: '',
      loggedIn: false,
      loading: true
    }
    this.updateActiveItemParent = this.updateActiveItemParent.bind(this);
    this.validateRole = this.validateRole.bind(this);
  }

  componentDidMount() {
    this.setState({
      loggedIn: true,
      user: {},
      activeItem: 'HOME'
    })


    //this.setState({loading: true})
    // auth().onAuthStateChanged(user => {
    //   //this.setState({ loading: false })
    //   if (user) {
    //     console.log("User logged in")
    //     this.setState({
    //       loggedIn: true,
    //       user: user
    //     })
    //     console.log(user)

    //     base
    //       .fetch(`roles/${user.uid}`, {
    //         context: this
    //       })
    //       .then(data => {
    //         if (data.uid) {
    //           this.setState({
    //             userRole: data.role,
    //             loading: false
    //           })
    //         } else {
    //           this.setState({
    //             userRole: '',
    //             loading: false
    //           })
    //         }
    //       })
    //       .catch(err => console.log(err))
    //   } else {
    //     console.log("User not logged in, redirect to Sign In")
    //     this.setState({
    //       loggedIn: false,
    //       user: {},
    //       userRole: '',
    //       loading: false
    //     })
    //     if(!(window.location.pathname === "/")){
    //       window.location = "/"
    //     }
    //   }
    // })
  }

  validateRole (someArg) {
    if(someArg.toLowerCase().indexOf('bu') != -1) {
      this.setState({
        user: {
          name: someArg
        },
        userRole: 'BUYER'
      })

      localStorage.setItem('userRole', 'BUYER')
    }
    else if(someArg.toLowerCase().indexOf('se') != -1) {
      this.setState({
        user: {
          name: someArg
        },
        userRole: 'SELLER'
      })
      localStorage.setItem('userRole', 'SELLER')
    }
    else if(someArg.toLowerCase().indexOf('treasury') != -1) {
      this.setState({
        user: {
          name: someArg
        },
        userRole: 'TREASURY'
      })
      localStorage.setItem('userRole', 'TREASURY')
    }
    else {
      // No user role found
    }
  }

  updateActiveItemParent(val) {
    this.setState({
      activeItem: val
    })    
    // this.render()
  }
  showRightTab(item) {
    let arr = [],
    pathNme = '/'+item;
    arr.push(
          <Route path={pathNme} render={(routeProps) => (
            <Land {...routeProps} user={this.state.user} userRole={this.state.userRole}  updateActiveItemParent={(val) => {this.updateActiveItemParent(val)}} activeItem={item}/>
          )}/> 
    )
    return arr;
  //   { activeItem === 'PURCHASE ORDERS' ?
  //   <Route path="/po" render={(routeProps) => (
  //     <Land {...routeProps} user={user} userRole={userRole}  updateActiveItemParent={(val) => {this.updateActiveItemParent(val)}} activeItem="PURCHASE ORDERS"/>
  //   )}/> : <div> </div>
  // }
  //   { activeItem === 'INVOICES' ?
  //   <Route path="/invoices" render={(routeProps) => (
  //     <Land {...routeProps} user={user} userRole={userRole}   updateActiveItemParent={(val) => {this.updateActiveItemParent(val)}} activeItem="INVOICES" />
  //   )}/> : <div> </div>
  // }
  //   { activeItem === 'PAYMENTS' ?
  //   <Route path="/payments" render={(routeProps) => (
  //     <Land {...routeProps} user={user} userRole={userRole}  updateActiveItemParent={(val) => {this.updateActiveItemParent(val)}} activeItem="PAYMENTS" />
  //   )}/> : <div> </div>
  // }
  //   { activeItem === 'DISPUTES' ?
  //   <Route path="/disputes" render={(routeProps) => (
  //     <Land {...routeProps} user={user} userRole={userRole}  updateActiveItemParent={(val) => {this.updateActiveItemParent(val)}} activeItem="DISPUTES" />
  //   )}/> : <div> </div>
  // }
  }
  render() {
    // const user = this.state.user
    // const userRole = this.state.userRole
    const activeItem = this.state.activeItem;

    const ManagerRole = ['SuperAdmin',  'StoreManager']

    return (
        <Router>
      <div className="App-Body">
        {/* {console.log(user,userRole)} */}
          <Route path="/" exact render={(routeProps) => (
            <SignIn {...routeProps} user={this.state.user} userRole={this.state.userRole} validateRole = {this.validateRole}  activeItem={activeItem}/>
          )}/>
          
          {/* <Route path="/dash" component={Authorization(Dash, ['SuperAdmin', 'StoreManager', 'FieldAgent', 'admin'], user, userRole)}/> */}
          {/* <Route path="/addagent" component={Authorization(AddAgent, ManagerRole, user, userRole)}/> */}

          <Route path="/signup" render={(routeProps) => (
            <SignUp {...routeProps} user={this.state.user} userRole={this.state.userRole} />
          )}/>
          {console.log(this,activeItem)}
         { this.showRightTab(activeItem)
          }
          
      </div>
        </Router>
    );
  }
}

export default App;
